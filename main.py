from network import WLAN, STA_IF
from time import sleep
from machine import I2C, Pin, deepsleep

from bme280 import BME280

from umqtt.simple import MQTTClient

import secrets
## REQUIRES FILE secrets.py WITH THESE:
# WLAN_SSID = 'AAA'
# WLAN_PASSWORD = 'BBB'
## http://worldtimeapi.org/timezones
# TIMEZONE = 'CCC'
# MQTT_USERNAME = "DDD"
# MQTT_KEY = "EEE"
# MQTT_SERVER = 'FFF'
# MQTT_FEED = 'GGG'


#################
# CONFIGURATION #
#################
DEVICE_ID = 'raspberry-pi-picow-1'
# 10 min = 600000 ms
_SLEEP_MS = const(600000)
_WLAN_WAIT_S = const(60)


##############################
# SENSOR CONNECTION AND READ #
##############################
# INITIALIZE I2C
i2c=I2C(0,sda=Pin(0), scl=Pin(1), freq=400000)
sensor = BME280(i2c=i2c)

#print(bme280_sensor.values)
#bme280_sensor_data = bme280_sensor.read_compensated_data()
#print('BME280 temperature: ' + str(0.01*bme280_sensor_data[0]))
#print('BME280 pressure: ' + str(0.01*bme280_sensor_data[1]/256))
#print('BME280 humidity: ' + str(bme280_sensor_data[2]/1024))

# READ THE TEMPERATURE
data = sensor.read_compensated_data()
temp = 0.01*data[0]
print('temperature:' + str(temp))


###################
# CONNECT TO WLAN #
###################
wlan = WLAN(STA_IF)
wlan.active(True)
wlan.connect(secrets.WLAN_SSID, secrets.WLAN_PASSWORD)

# WAIT FOR CONNECT OR FAIL
i = 0
while not wlan.isconnected():
    print('Waiting for WLAN connection... (' + str(i) + 's)')
    sleep(1)
    i += 1
    if i > _WLAN_WAIT_S:
        # WLAN CONECTION FAILS, SLEEP AND TRY AGAIN NEXT TIME
        print('WLAN connection failed. sleep...')
        wlan.disconnect()
        wlan.active(False)
        sleep(1)
        deepsleep(_SLEEP_MS)

# CONNECTION OK
status = wlan.ifconfig()
ip = status[0]
print(f'Connected to WLAN, ip address: {ip}')


#################################
# CONNECT TO MQTT BROKER/SERVER #
#################################
mqtt_client = MQTTClient(
    client_id = DEVICE_ID,
    server = secrets.MQTT_SERVER,
    port = 1883,
    user = secrets.MQTT_USERNAME,
    password = secrets.MQTT_KEY,
    keepalive = 0,
    ssl = False,
    ssl_params = [])
try:
    mqtt_client.connect()
except OSError as e:
    print('MQTT broker/server connect fail, sleep...')
    mqtt_client.disconnect()
    wlan.disconnect()
    wlan.active(False)
    print('Deep Sleep...')
    sleep(1)
    deepsleep(_SLEEP_MS) # milliseconds
    #sleep(DATA_SEND_INTERVAL_SECONDS)
    #machine.soft_reset()
print('Connected to %s MQTT Broker'%(secrets.MQTT_SERVER))

# SEND THE DATA
mqtt_client.publish(
    topic = secrets.MQTT_FEED,
    msg = str(temp),
    retain = False,
    qos = 0)
mqtt_client.disconnect()

# WLAN OFF
wlan.disconnect()
wlan.active(False)
sleep(1)

# DEEP SLEEP
print('Deep Sleep...')
deepsleep(_SLEEP_MS) # milliseconds
# LIGHTSLEEP runs better with Thonny
#machine.lightsleep(1000*SLEEP_SECONDS) # milliseconds
