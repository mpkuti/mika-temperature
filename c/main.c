/**
 * Copyright (c) 2022 Raspberry Pi (Trading) Ltd.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */
#include <stdio.h>
#include "pico/stdlib.h"
#include "pico/cyw43_arch.h"

#include "secrets.h"

int i = 0;

int main() {
    stdio_init_all();

    //if (cyw43_arch_init_with_country(CYW43_COUNTRY_FINLAND)) {
    if (cyw43_arch_init()) {
        printf("WiFi init failed");
        return -1;
    }
    printf("WiFi init done\n");

    cyw43_arch_enable_sta_mode();

    if (cyw43_arch_wifi_connect_timeout_ms(ssid, pass, CYW43_AUTH_WPA2_AES_PSK, 50000)) {
        printf("WiFi connection failed\n");
        return 1;
    }
    printf("WiFi connected\n");

    while (true) {
        printf("COUNTER: %d\n", i);
        cyw43_arch_gpio_put(CYW43_WL_GPIO_LED_PIN, 1);
        sleep_ms(100);
        cyw43_arch_gpio_put(CYW43_WL_GPIO_LED_PIN, 0);
        sleep_ms(200);
        i++;
    }
}
