# mika-temperature


### FIRMWARE

Raspberry Pi PicoW firmware: rp2-pico-w-20220914-unstable-v1.19.1-409-g0e8c2204d.uf2

Firmware with deep sleep (not used in this project yet):
[https://github.com/ghubcoder/micropython-pico-deepsleep/releases/tag/v1.2-pico-deepsleep](https://github.com/ghubcoder/micropython-pico-deepsleep/releases/tag/v1.2-pico-deepsleep)


### Install BME280 driver (same instructions work for *picozero* if needed)

Thonny -> Tools -> Package Manager, search for *bme280* -> select *micropython_bme280*.

Driver web page: [https://github.com/SebastianRoll/mpy_bme280_esp8266](https://github.com/SebastianRoll/mpy_bme280_esp8266)


### Install MQTT library

Run these on the Raspberry Pi PicoW:

    import network
    wlan = network.WLAN(network.STA_IF)
    wlan.active(True)
    wlan.connect("Wi-Fi AP","PASSWORD")
    import upip
    upip.install("micropython-umqtt.simple")

source: [https://www.tomshardware.com/how-to/send-and-receive-data-raspberry-pi-pico-w-mqtt](https://www.tomshardware.com/how-to/send-and-receive-data-raspberry-pi-pico-w-mqtt)


### BATTERY USAGE MEASUREMENTS

Wait between data sends: 10 min

Using time.sleep(): 15 h

Using machine.lightsleep(): 30 h

Using machine.deepsleep(): ? h
